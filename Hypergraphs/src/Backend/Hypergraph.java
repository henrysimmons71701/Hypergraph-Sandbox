package Backend;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Hypergraph {			// comment
	String name;
	ArrayList<edge> edgeSet;
	ArrayList<vertex> vertexSet;
	
	public Hypergraph() {
		edgeSet = new ArrayList<>();
		vertexSet = new ArrayList<>();
	}
	
	public void addEdge(vertex v1, vertex v2, vertex v3, vertex v4) {
		edgeSet.add(new edge(v1, v2, v3, v4));
		if(!vertexSet.contains(v1))
			vertexSet.add(v1);
		if(!vertexSet.contains(v2))
			vertexSet.add(v2);
		if(!vertexSet.contains(v3))
			vertexSet.add(v3);
		if(!vertexSet.contains(v4))
			vertexSet.add(v4);
	}
	
	public void addVertex(int x, int y, String name) {
		vertexSet.add(new vertex(x, y, name));
	}
	
	public int getEdgeSetSize() {
		return edgeSet.size();
	}
	public int getVertexSetSize() {
		return vertexSet.size();
	}
	
	public ArrayList<vertex> VetexSet() {
		return vertexSet;
	}
}


