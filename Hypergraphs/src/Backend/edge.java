package Backend;

import java.util.ArrayList;

class edge {
	
	int size;
	ArrayList<vertex> vertices = new ArrayList<>();
	
	edge(int size){
		this.size = size;
		for(int i = 0; i < size; i++) {
			vertices.add(new vertex());
		}
	}
	
	edge(vertex v1, vertex v2) {
		size = 2;
		addVertex(v1);
		addVertex(v2);
	}
	
	edge(vertex v1, vertex v2, vertex v3, vertex v4) {
		size = 4;
		addVertex(v1);
		addVertex(v2);
		addVertex(v3);
		addVertex(v4);
	}
	
	void addVertex(vertex v1) {
		vertices.add(v1);
		v1.setEdge(this);
	}
}