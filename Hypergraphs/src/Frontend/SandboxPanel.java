package Frontend;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import Backend.Hypergraph;
import Backend.vertex;


public class SandboxPanel extends JPanel implements ActionListener {

	int width = 600;
	int height = 600;
	int boarderSize = 10;
	int radius = 5;
	Hypergraph H;
	
	
	SandboxPanel() {
		this.setPreferredSize(new Dimension(width + 2*boarderSize, height + 2*boarderSize));
		this.setBackground(Color.LIGHT_GRAY);
		this.setFocusable(true);
		this.addMouseListener(new MyMouseAdapter());
		start();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		drawAll(g);
	}
	public void drawAll(Graphics g) {
		g.setColor(Color.white);								// white background
		g.fillRect(boarderSize, boarderSize, width, height);
		
		g.setColor(Color.black);
		vertex v;
		for(int i = 0; i < H.getVertexSetSize(); i++) {
			v = H.VetexSet().get(i);
			fillCircle(g, v.getX(), v.getY(), radius);
		}
	}
	
	
	private void drawCircle(Graphics g, int x, int y, int radius) {
		g.drawOval(x + width/2 - radius + boarderSize,
				y + height/2 - radius + boarderSize, 2*radius, 2*radius);
	}
	private void fillCircle(Graphics g, int x, int y, int radius) {
		g.fillOval(x + width/2 - radius + boarderSize,
				y + height/2 - radius + boarderSize, 2*radius, 2*radius);
	}
	
	
	private void start() {
		// TODO Auto-generated method stub
		H = new Hypergraph();
		H.addVertex(0, 0, "one");
	}





	public class MyMouseAdapter extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
	////////////////// Setters and getters /////////////////////
	public int getSandBoxWidth() {
		return width;
	}
	
	public int getSandBoxBoarderSize() {
		return boarderSize;
	}
}
