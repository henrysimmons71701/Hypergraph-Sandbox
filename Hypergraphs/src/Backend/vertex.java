package Backend;

public class vertex {
	String name;
	int x;
	int y;
	int id;
	edge edge;
	
	vertex(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	vertex(int x, int y, String name) {
		this.x = x;
		this.y = y;
		this.name = name;
	}
	
	vertex(){
		
	}
	
	void setEdge(edge edge) {
		this.edge = edge;
	}

	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
}
