package Frontend;

import javax.swing.JFrame;


public class Display extends JFrame{
	public static void main(String[] args) {
		Display display = new Display();
	}
	
	Display() {
		
		SandboxPanel sandboxPanel = new SandboxPanel();
		this.add(sandboxPanel);
		
		MenuPanel menuPanel = new MenuPanel();
		this.add(menuPanel);
		
		this.setTitle("Hypergraph Sandbox");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.pack();
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		
	}
}


